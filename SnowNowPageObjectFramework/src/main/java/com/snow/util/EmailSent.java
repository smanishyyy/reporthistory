package com.snow.util;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

public class EmailSent {

	public static void main(String arg[]) throws EmailException {

		String path = "C:/Users/chaurma/Downloads/Project/STRY0020317/SnowNowPageObjectFramework/target/Extent Cucumber-reports/Report.html";
//		sendEmail(path);
	}

	public static void sendEmail(String text, String strFailCount, String strPassCount) throws EmailException {
		EmailAttachment attachment = new EmailAttachment();
		attachment.setPath(text);
		attachment.setDisposition(EmailAttachment.ATTACHMENT);

		// Create the email message
		MultiPartEmail email = new MultiPartEmail();
		email.setHostName("mail.manulife.com");
		email.addTo("MChaurasia@jhancock.com", "Manish Chaurasia");
		email.setFrom("MChaurasia@jhancock.com", "Manish Chaurasia");
		email.setSubject("Execution of MF-IBM Change");
		email.setMsg("Please finde the execution status \nPassed test case count : "
		+strPassCount+"\n"+"Failed test case count : "+strFailCount+"\n"+"\n"+"Please do let me know if any concern"+"\n"+"\n"+
				"Thanks and Regards"+"\n"+"Manish Chaurasia"+"\n"+"MChaurais@jhancock.com | johnhancock.com");
		// add the attachment
		email.attach(attachment);
		email.send();

	}
}
